---
# Display name
name: Mário S. Alvim
title: Mário S. Alvim

# Username (this should match the folder name)
authors:
- msalvim

# Is this the primary user of the site?
superuser: false

# Role/position
role: Assistant Professor

# Organizations/Affiliations
organizations:
- name: DCC/UFMG
  url: "http://dcc.ufmg.br"

# Short bio (displayed in user profile at end of posts)
# bio: I am an assistant professor at the Computer Science Department (DCC) of Universidade Federal de Minas Gerais (UFMG), in Belo Horizonte, Brazil.

interests:
- Quantitative Information Flow
- Privacy
- Foundations of Computer Security
- Formal Methods

education:
  courses:
  - course: PhD in Computer Science
    institution: LIX - École Polytechnique, France
    year: 2011
  - course: MSc in Computer Science
    institution: Universidade Federal de Minas Gerais (UFMG), Brazil
    year: 2008
  - course: BSc in Computer Science
    institution: Universidade Federal de Minas Gerais (UFMG), Brazil
    year: 2005

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: 'mailto:msalvim@dcc.ufmg.br'  # For a direct email link, use "mailto:test@example.org".
- icon: link
  icon_pack: fas
  link: "https://homepages.dcc.ufmg.br/~msalvim/"
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=9r8YfCgAAAAJ
- icon: lattes
  icon_pack: ai
  link: http://lattes.cnpq.br/1397639761790594
- icon: cv
  icon_pack: ai
  link: https://homepages.dcc.ufmg.br/~msalvim/files/msalvim_cv%5bshort%5d.pdf
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "msalvim@dcc.ufmg.br"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Faculty
---

<!-- markdownlint-disable MD041 -->
