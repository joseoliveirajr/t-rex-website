---
title: Áreas de Pesquisa
summary: ""
date: "2020-12-10T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

slug: areas-de-pesquisa

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---

## Privacidade e Controle de Divulgação Estatística

Em muitas situações, como em censos nacionais e estudos médicos, é desejável divulgar informações estatísticas sobre uma população e, ao mesmo tempo, proteger a privacidade dos indivíduos participantes da amostra. Em um estudo médico, por exemplo, pode-se desejar que a divulgação permita ao consumidor de dados aprender informações estatísticas sobre a população (por exemplo, qual a prevalência de determinada doença na população), Sem permitir violações de privacidade (por exemplo, o usuário aprender se um determinado indivíduo tem ou não a doença). Conciliar requisitos de utilidade com privacidade em uma liberação de dados é, no entanto, uma tarefa não trivial. As áreas de conhecimento de privacidade e de controle de divulgação estatística lidam com o equilíbrio entre privacidade e utilidade na divulgação de dados, aplicando técnicas sofisticadas como privacidade diferencial e suas variantes.

## Fluxo Quantitativo de Informação (QIF)

Fluxo de informação quantitativo (*Quantitative Information Flow* - QIF, no original em inglês) é a área de conhecimento que se preocupa em medir e controlar a quantidade de informação que flui de uma fonte (que conhece a informação) para um alvo (que ainda não a conhece). Em alguns casos, o fluxo de informação deve ser facilitado (por exemplo, quando dados de treino fornecem informação útil para um algoritmo de aprendizado de máquina), ao passo que em outros casos deve ser evitado (por exemplo, ao proteger dados bancários de um usuário na Internet). Normalmente, porém, os objetivos são uma mistura cuidadosa dos dois: permitir que a informação flua para aqueles que precisam conhecê-la, mas escondê-la daqueles que não devem tê-la. O arcabouço QIF permite a quantificação precisa do fluxo de informação em sistemas computacionais, incluindo a medição de vazamentos em programas computacionais (por exemplo, sistemas eleitorais), em protocolos de comunicação (como o TOR), na divulgação de informações estatísticas (por exemplo, censo governamental ou pesquisa médica), entre muitos outros.

## Teoria Algébrica de Grafos

Estudo de como as propriedades combinatórias de grafos se relacionam com objetos algébricos associados ao grafo. Exemplos de tais objetos são as matrizes de adjacência, alguns polinômios, como o característico, de emparelhamento ou cromático, e também o grupo de automorfismos do grafo. Resultados típicos dessa área usam métodos algébricos para encontrar aplicações úteis à teoria dos grafos. Tenho os interesses mais gerais possíveis nesta área.

## Computação Quântica

Pense em um bit. Ou 0, ou 1. Imagine agora ser possível fazer computação não com bits, mas sim com qubits, que são espaços vetoriais sobre os complexos de dimensão 2. Ao invés de manipular algo para ser 0 ou 1, você vai manipular algo para variar entre todas as possíveis retas em um plano. As aplicações são inúmeras, e há muitos aspectos a serem estudados. Os que mais me interessam são aqueles que permitem a aplicação de teoria espectral de grafos à computação quântica.
