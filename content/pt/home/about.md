+++
widget = "hero"
headless = true  # This file represents a page section.
active = true
weight = 10

# ... Put Your Section Options Here (title etc.) ...

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
hero_media = "logo.png"

# height = 500

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.

# Note. An optional note to show underneath the links.
+++

# Sobre nós

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum posuere, neque quis ullamcorper consequat, felis eros bibendum libero, vel volutpat quam justo ac leo. Phasellus ut leo vel sem tristique iaculis non non velit. Maecenas quis consequat augue. Nulla blandit purus sed suscipit suscipit.
